﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryStudents
{
    public abstract class BankClient : IBankAccount, IComparable
    {
        public BankClient(int id, 
            string fullName, 
            string shortName = "", 
            float balance = 0)
        {
            Id = id;
            FullName = fullName;
            ShortName = shortName;
            Balance = balance;
        }
        #region Свойства
        public int Id { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public float Balance { get; set; }
        #endregion
        public virtual float IncreaseBalance(float amount)
        {
            var n = amount > 0 ? amount : 0; 
            return Balance += n;
        }

        public float ReduceBalance(float amount)
        {
            var n = amount > 0 ? amount : 0;
            return Balance -= amount;
        }

        public abstract void Display();

        public int CompareTo(object obj)
        {
            BankClient client = (BankClient)obj;
            return this.Id > client.Id ? 1 : -1;
        }

        public static bool BalancePredicate(IBankAccount account)
        {
            return account.Balance > 17000;
        }
        public static bool NameLenghtPredicate(IBankAccount account)
        {
            return account.FullName.Length > 17;
        }
    }
}
