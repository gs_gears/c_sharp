﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryStudents
{
    public class Student
    {
        #region Поля
        /// <summary>
        /// Номер зачетной книжки
        /// </summary>
        private int id;
        /// <summary>
        /// Полное ФИО студента
        /// </summary>
        private string name;
        /// <summary>
        /// Средний балл за сессию
        /// </summary>
        private float avgMark;

        public Student(int id, string name, float avgMark)
        {
            this.id = id;
            this.name = name ?? throw new ArgumentNullException(nameof(name));
            this.avgMark = avgMark;
        }

        public Student()
        {
        }
        #endregion
        #region Свойства
        /// <summary>
        /// Номер зачетной книжки
        /// </summary>
        public int Id { get => id; set => id = value; }
        /// <summary>
        /// Полное ФИО студента
        /// </summary>
        public string Name { get => name; set => name = value; }
        /// <summary>
        /// Средний балл за сессию
        /// </summary>
        public float AvgMark { get => avgMark; set => avgMark = value; }
        #endregion

        #region Методы класса
        public override string ToString()
        {
            return $"Номер зачетной книжки: {id},\n" +
                $"Полное ФИО: {name},\n" +
                $"Средний балл: {avgMark}";
        }

        public override bool Equals(object obj)
        {
            var student = obj as Student;
            if (student is Student)
                return this.id == student.id && 
                    this.name == student.name;
            else
                return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, name, avgMark);
        }

        public static bool operator ==(Student left, Student right)
        {        
            return EqualityComparer<Student>.Default.Equals(left, right);
        }

        public static bool operator !=(Student left, Student right)
        {
            return !(left == right);
        }

        public static void Hello(Student stud)
        {
            Console.WriteLine($"Привет {stud.name}");
        }
        #endregion
    }
}
