﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryStudents
{

    public delegate bool FilterDelegate(IBankAccount bankAccount);
    public class Filtration
    {
        public static void FilterClients(IBankAccount[] clients, FilterDelegate filter)
        {
            IBankAccount[] accounts = new IBankAccount[clients.Length];
            int count = 0;
            foreach(var client in clients)
            {
                if (filter(client))
                {
                    accounts[count] = client;
                    count++;
                }
            }
            DisplayClients(accounts);
        }

        private static void DisplayClients(IBankAccount[] clients)
        {
            foreach (var client in clients)
            {
                if (client != null)
                {
                    Console.WriteLine(client.ToString());
                }              
            }
        }
    }
}
