﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryStudents
{
    public class BankClientStudent : BankClient
    {
        /// <summary>
        /// Средний балл за сессию
        /// </summary>
        private float avgMark;  
        public float AvgMark { get => avgMark; set => avgMark = value; }
        public BankClientStudent(int id, 
            string fullName,
            float avgMark,
            string shortName = "", 
            float balance = 0) 
            : base(id, fullName, shortName, balance)
        {
            this.avgMark = avgMark;
        }

        public override float IncreaseBalance(float amount)
        {
            int bonus = 2000;
            if (AvgMark == 5.0f)
                return base.IncreaseBalance(amount + bonus);   
            else
                return base.IncreaseBalance(amount);
        }

        public override string ToString()
        {
            return $"Номер клиента {Id}; " +
                $"полное имя {FullName}\n" +
                $"Ср. оценка {AvgMark}; " +
                $"баланс {Balance}";
        }

        public override void Display()
        {
            Console.WriteLine($"Номер клиента {Id}; " +
                $"полное имя {FullName}\n" +
                $"Ср. оценка {AvgMark}; " +
                $"баланс {Balance}");
        }
    }
}
