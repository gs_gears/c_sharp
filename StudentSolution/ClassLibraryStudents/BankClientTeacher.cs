﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryStudents
{
    public class BankClientTeacher : BankClient
    {
        private string scienceRank;

        public string ScienceRank { get => scienceRank; set => scienceRank = value; } 

        public BankClientTeacher(int id, string fullName, string scienceRank = "no",
            string shortName = "", float balance = 0) 
            : base(id, fullName, shortName, balance)
        {
            this.scienceRank = scienceRank;
        }

        public override float IncreaseBalance(float amount)
        {
            int bonus = 5000;
            if (scienceRank.Contains("PhD"))
                return base.IncreaseBalance(amount + bonus);
            else
                return base.IncreaseBalance(amount);
        }

        public override string ToString()
        {
            return $"Номер клиента {Id}; " +
                $"полное имя {FullName}\n" +
                $"Уч. степень {ScienceRank}; " +
                $"баланс {Balance}";
        }

        public override void Display()
        {
            Console.WriteLine($"Номер клиента {Id}; " +
                $"полное имя {FullName}\n" +
                $"Уч. степень {ScienceRank}; " +
                $"баланс {Balance}");
        }
    }
}
