﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClassLibraryStudents
{
    public interface IBankAccount
    {
        int Id { get; set; }
        string FullName { get; set; }
        string ShortName { get; set; }
        float Balance { get; set; }
        /// <summary>
        /// Метод уменьшения баланса
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        float ReduceBalance(float amount);
        /// <summary>
        /// Метод увеличения баланса
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        float IncreaseBalance(float amount);
    }
}
