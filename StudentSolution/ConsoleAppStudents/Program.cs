﻿using System;
using ClassLibraryStudents;

namespace ConsoleAppStudents
{
    class Program
    {
        int count = 5;
        static void Main(string[] args)
        {
            BankClientStudent[] clientStudents = new BankClientStudent[]{
                new BankClientStudent(1, "Иванов Иван", 4.5f),
                new BankClientStudent(4, "Петров Иван", 5.0f),
                new BankClientStudent(3, "Иванов Иван", 4.9f),
                new BankClientStudent(2, "Иванов Иван", 2.5f)
            };

            BankClientTeacher[] clientTeachers = new BankClientTeacher[]
            {
                new BankClientTeacher(1, "Петров Петр", "PhD"),
                new BankClientTeacher(1, "Сидоренко Петр sdsfgfdwerwewrew"),
                new BankClientTeacher(1, "Петрова Ирина", "PhD"),
                new BankClientTeacher(1, "Сидоров Петр")
            };
            //ClientIncrease(clientStudents, 2200);
            //DisplayClient(clientStudents);
            //Array.Sort<IBankAccount>(clientStudents);
            //DisplayClient(clientStudents);

            ClientIncrease(clientTeachers, 15200);
            // DisplayClients(clientTeachers);
            FilterDelegate filter = delegate (IBankAccount account) { 
                return account.Balance > 17000;
            };
            Filtration.FilterClients(clientTeachers,
                (IBankAccount account) => { return account.Balance > 17000; });
            
            //IBankAccount bank = new BankClientStudent(1, "Иванов Иван", 4.5f);
            //var client = bank as BankClientStudent;
            //Console.WriteLine(client.ToString());

        }

        private static void DisplayClients(IBankAccount[] clients)
        {
            foreach (var client in clients)
            {
                Console.WriteLine(client.ToString());
            }
        }

        private static void ClientIncrease(IBankAccount[] clients, 
            float amount)
        {
            foreach (var client in clients)
            {
                client.IncreaseBalance(amount);
            }
        }
    }
}
